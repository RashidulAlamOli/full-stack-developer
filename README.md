# Fullstack Developer Challenge

## Requirements
* Use NodeJS, .Net Core or language of your choice for backend. 
* Use JS framework (Vue, React, Angular).
* Use CSS preprocessor (SCSS, SASS, LESS).
* Use MySQL or other SQL Database for data store.
* Use task-runner, to compile your css/js (Gulp, Webpack or Grunt), but do not commit the build.

## Completed
* .Net Core 3.1 for backend. 
* Angular 10 for frontend
* Sql Server 2018

## What I couldn't
* Task-runner (Because I am not familiar with gulp or webpack as I use mostly ng-cli for build and minification)
* Not completed ui project ( There is too much room for improve. But i couldn't manage time and i am not good at html or css )


## How to run the project
* For backend, go to root folder.Use .net cli and hit "dotnet build" and then "dotnet run". It will autometically create db in your local bd.
* For frondend, go to root folder and open command promt. Just do "npm install" and then "ng serve" :)


